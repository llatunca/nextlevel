# Aprendiendo sobre brindar Primeros Auxilios
*******
En cualquier momento, usted o alguien a su alrededor podría experimentar una lesión o enfermedad. Con los primeros auxilios básicos, puede evitar que un accidente menor empeore. En el caso de una emergencia médica grave, incluso puede salvar una vida.
Consisten en el apoyo inicial brindado a alguien en medio de una emergencia médica. Este apoyo podría ayudarlos a sobrevivir hasta que llegue la ayuda profesional. En otros casos, los primeros auxilios consisten en la atención brindada a alguien con una lesión menor. Por ejemplo, los primeros auxilios suelen ser todo lo que se necesita para tratar quemaduras leves, cortes y picaduras de insectos.
*******
## Temario | Silabo

1. **En el lugar de trabajo**   

2. **Para actividades al aire libre** 

3. **Lesiones deportivas**

4. **De emergencia**

*******
## Material 
- [Lista de videos 1](https://www.youtube.com/user/stjohnambulance/playlists)
- [Lista de video 2](https://www.youtube.com/watch?v=ysIiVjh_VcE&list=PLfIjAEIO4JvRrcKkRg2Q951vWKrKKHesN&index=4)
- [Lista de video 3](https://www.youtube.com/watch?v=suNiaxLEr2Y&list=PLWgKm7kgxzCu2hHXJhvroQJRALkyqGq9a)
