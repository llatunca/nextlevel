# Aprendiendo R

*******
R es un lenguaje poderoso para el análisis de datos, visualización de datos, aprendizaje automático, estadísticas. Originalmente desarrollado para programación estadística, ahora es uno de los lenguajes más populares en ciencia de datos.

*******
## Temario | Silabo

1. **Conceptos básicos de R**  
- [ ] Matemáticas, variables y cadenas
- [ ] Vectores y factores
- [ ] Operaciones vectoriales

2. **Estructuras de datos en R**
- [ ] Matrices y matrices
- [ ] Liza
- [ ] Marcos de datos 

3. **Fundamentos de programación R**
- [ ] Condiciones y bucles 
- [ ] Funciones en R
- [ ] Objetos y clases
- [ ] Depuración

4. **Trabajando con datos en R**
- [ ] Lectura de archivos CSV y Excel 
- [ ] Leer archivos de texto 
- [ ] Escribir y guardar objetos de datos para archivar en R

5. **Cadenas y fechas en R** 
- [ ] Operaciones de cadena en R 
- [ ] Expresiones regulares 
- [ ] Fechas en R

*******
## Material 
- [Curso gratis Cognitive Class](https://cognitiveclass.ai/courses/r-101)
