# Aprendiendo sobre la Inteligencia Espiritual
*******
La inteligencia espiritual o trascendente es la que nos permite entender el mundo, a los demás y a nosotros mismos desde una perspectiva más profunda y más llena de sentido, ya que, nos ayuda a trascender el sufrimiento y a ver más allá del mundo material, entrando en esa amplia e interconectada dimensión espiritual tan alejada del mundo material en el que habitualmente nos desenvolvemos. Muchos autores la consideran el tipo de inteligencia más elevada de todos. La Inteligencia Espiritual se refiere a la lucha humana por encontrar sus mayores talentos y potencial de curación y felicidad se consiguen. Es vital para su supervivencia y bienestar.
*******
## Libros Recomendados
- [ ] Vístete de Poder - Wendy Bretón
- [ ] Espiritual Mente - Gustavo Daniel Romero Santos y Ana Kelleyian
- [ ] Francis Fukuyama - Trust

*******
## Mayor Informacón
Dudas y consultas [Link](https://www.instagram.com/ieeecomsocuch/)
