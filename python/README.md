# Aprendiendo sobre python
*******
Python es un lenguaje de programación muy poderoso utilizado para muchas aplicaciones diferentes. Con el tiempo, la gran comunidad en torno a este lenguaje de código abierto ha creado bastantes herramientas para trabajar de manera eficiente con Python. En los últimos años, se han creado una serie de herramientas específicas para la ciencia de datos. Como resultado, analizar datos con Python nunca ha sido tan fácil. Es bueno comenzar desde el principio, con aritmética y variables básicas, y aprenderá a manejar estructuras de datos, como listas de Python, matrices Numpy y marcos de datos de Pandas. Luego sobre las funciones de Python y el flujo de control. Además, de las visualizaciones de datos con Python y creará sus propias visualizaciones impresionantes basadas en datos reales.
*******
## Temario | Silabo
1. **Perfil básico de Python** 
- [ ] Conceptos básicos de Python
- [ ] Listas de Python
- [ ] Funciones y paquetes
- [ ] Numpy
- [ ] Matplotlib
- [ ] Control de flujo y Pandas
2. **Perfil de Ciencia de Datos** 
3. **Perfil de procesamiento de imagenes** 
4. **Perfil en salud** 
5. **Perfil en Drones** 
6. **Perfil en AI** 
*******
## Libros Recomendados 
- [ ] [The Quick Python Book](http://www.astro.caltech.edu/~vieira/The_Quick_Python_Book.pdf)
- [ ] Expert Python Programming: Become a Master in Python by Learning Coding Best
*******
## Cursos gratis recomendados
- [Curso gratis EDX & Microsoft](https://prod-edx-mktg-edit.edx.org/course/introduction-to-python-for-data-science-4)
*******
## Material 
- [Ejercios de Python en español](http://pywombat.com/)
- [Real Python Tutorials](https://realpython.com/)
- [Curso gratis GDrive](https://drive.google.com/drive/folders/1QMm_0B9DswJKS3mNRDTnUk3qKGLwSKMD?fbclid=IwAR2HK7e7TnBvrFUjopLPdL3E5zLkaDVbrhmYQOA9kGh_mgIiFVpniCOplEs)
