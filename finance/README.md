# Aprendiendo sobre Finanzas personales(Inteligencia Financiera)
*******
Las finanzas personales son la gestión financiera que realiza una unidad individual o familiar para presupuestar , ahorrar y gastar recursos monetarios a lo largo del tiempo, teniendo en cuenta diversos riesgos financieros y eventos de la vida futura. Al planificar las finanzas personales, el individuo consideraría la idoneidad para sus necesidades de una gama de productos bancarios o inversiones de capital privado y seguros productos o la participación y el seguimiento de y/o patrocinados por el empleador planes de jubilación , seguridad social beneficios, y el impuesto sobre la renta de gestión.La inteligencia financiera no es una habilidad innata, sino que es un conjunto de habilidades aprendidas que se pueden desarrollar en todos los niveles.
*******
## Material 
- [Canal de YouTube: Emprende aprendiendo](https://www.youtube.com/channel/UCyM-2pRapEv6V2q7UNO9icg)
- [Canal de Youtube: La Agenda de Karen](https://www.youtube.com/user/karemsuarezv)
- [Canal de Youtube: MoneyGamia](https://www.youtube.com/channel/UCiLNA3gRwiRukTJee1cEj1A/videos)
- [Canal de Youtube: Aprendiz Financiero](https://www.youtube.com/channel/UCLYoURyGHMUuQ5YrfX55PWg/videos)

*******
## Libros Recomendados
- [ ] El obstáculo es el camino - LRyan Holiday
- [ ] Padre Rico, Padre Pobre - Robert Kiyosaki y Sharon Lechter
- [ ] El hombre más rico de Babilonia - George S. Clason
- [ ] Contabilidad y finanzas para no financieros - Oriol Amat
- [ ] El poder de los hábitos - Charles Duhigg
- [ ] El vendedor más grande del mundo - Og Mandino
