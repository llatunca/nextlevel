# Aprendiendo sobre Inteligencia Emocional
*******
La inteligencia emocional o EI es la capacidad de comprender y manejar sus propias emociones y las de las personas que lo rodean. Las personas con un alto grado de inteligencia emocional saben lo que sienten, lo que significan sus emociones y cómo estas emociones pueden afectar a otras personas.
*******
## Libros Recomendados
- [ ] Una mochila para el universo - Elsa Punset
- [ ] Cómo hacer amigos e influir sobre las personas - Dale Carnegie 
- [ ] Emocionario. Di Lo Que Sientes - Cristina Núñez Pereira y Rafael Romero
- [ ] Despertando al gigante interior - Tony Robbins
- [ ] Decídete - Chip y Dan Heath
- [ ] El proceso para convertirse en persona - Carl Rogers
- [ ] Introducción a la psicología analítica - Jung, Carl Gustav
- [ ] Principios de Aprendizaje y Conducta - Michael Domjan

*******
## Material 
- [Canal de YouTube: Mindalia Televisión ](https://www.youtube.com/user/mindaliacom/)
- [Canal de Youtube: Sergi Rufi REAL](https://www.youtube.com/channel/UCjjrc73Y9KzcH2loXzOzy1A)


