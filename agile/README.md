# Aprendiendo sobre Metodologías ágiles
*******
Son métodos que ayudan a los equipos a responder a la imprevisibilidad de la construcción de productos o tecnologia en general. Utiliza secuencias de trabajo iterativas incrementales que se conocen comúnmente como sprints.
*******
## Temario | Silabo

1. **SCRUM**
- [ ] Interesados e historia de usuario
- [ ] Roles y eventos de Scrum
- [ ] El proyecto Agil

2. **KANBAN**
- [ ] Procesos del flujo de trabajo
- [ ] Métricas visuales
- [ ] Entrega continua

3. **Extreme Programming (XP)**
- [ ] Codificación
- [ ] Pruebas para comprobar

4. **Agile Inception**
- [ ] Clarificar cuestiones
- [ ] Elevator pitch
- [ ] Trabajar en un Host

5. **Design Sprint** 
- [ ] Ideas
- [ ] Idear
- [ ] Crear prototipos
- [ ] Probar un concepto
- [ ] Solución

6. **Lean**
- [ ] Implementacion de metodología de trabajo
- [ ] Miembros involucrados
- [ ] Motivación y sentido

*******
## Bibliografía 
- [Guía de práctica ágil](https://www.pmi.org/pmbok-guide-standards/practice-guides/agile)

## Material
- [Curso gratis Metodologias ágiles + Scrum](https://openwebinars.net/cursos/metodologias-agiles/)
