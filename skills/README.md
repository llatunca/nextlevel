# Aprendiendo sobre habilidades blandas
*******
El término habilidades blandas (soft skills) se refiere a los rasgos, características y competencias personales que informan cómo un individuo se relaciona con los demás y, a menudo, se usa como sinónimo de habilidades sociales o habilidades interpersonales.
*******
## Libros Recomendados
- [ ] Soluciona situaciones difíciles con la Escucha Activa - Francesc Selva
- [ ] Reuniones Eficaces - Francesc Selva

*******
## Mayor Informacón
Dudas y consultas [Link](https://www.instagram.com/ieeecomsocuch/)
